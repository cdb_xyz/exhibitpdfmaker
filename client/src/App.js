import React, { Component } from "react";
import pdfMake from "pdfmake/build/pdfmake";
import vfsFonts from "pdfmake/build/vfs_fonts";
import Button from "@material-ui/core/Button";

const createAndDownloadPDF = exhibits => {
    const { vfs } = vfsFonts.pdfMake;
    pdfMake.vfs = vfs;
    const baseStyle = { fontSize: 65, alignment: "center", margin: [ 0, 200, 0, 0 ] };
    const withBreak = { pageBreak: "after" };
    const formattedExhibits = exhibits.map(e => {
        const str = e.toString();
        return str.length > 1 ? str : `0${str}`;
    });
  
    const documentDefinition = {
        pageSize: "letter",
        pageOrientation: "portrait",
        content: formattedExhibits.map((m, i) => {
            return  i + 1 < formattedExhibits.length ?
                { text: `EXHIBIT ${m}`, ...baseStyle, ...withBreak } :
                { text: `EXHIBIT ${m}`, ...baseStyle };
        })
    };

    pdfMake.createPdf(documentDefinition).download(`${Date.now()}_exhibits.pdf`);
};

class App extends Component {
  state = {
      exhibits: [],
      minExhibit: 0,
      maxExhibit: 1
  }

  getPDF = () => {
      const exhibits = [];
      for (let i = this.state.minExhibit; i <= this.state.maxExhibit; i++) {
          exhibits.push(i);      
      }
      createAndDownloadPDF(exhibits);
  }

  handleChange = e => {
      const state = {};
      state[e.target.name] = e.target.value;
      this.setState({ ...state });
  }
   
  render() {
      return (
          <div className="App" style={{ marginLeft: "25%", width: "50%" }}>
              <h1>Create Exhibit PDF</h1>
              <p>Start Exhibit <input value={this.state.minExhibit} name="minExhibit" style={{ marginLeft: 10, width: 25 }} onChange={this.handleChange}/> </p>
              <p>End Exhibit <input value={this.state.maxExhibit} name="maxExhibit" style={{ marginLeft: 10, width: 25 }} onChange={this.handleChange}/> </p>
              <Button onClick={this.getPDF}>Download</Button>
          </div>
      );
  }
}

export default App;
